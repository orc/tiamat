import datetime
import os
import pathlib
import shutil

import nox
from nox.command import CommandFailed

# Nox options
#  Reuse existing virtualenvs
nox.options.reuse_existing_virtualenvs = True
#  Don't fail on missing interpreters
nox.options.error_on_missing_interpreters = False

# Python versions to test against
PYTHON_VERSIONS = ("3", "3.9", "3.10")
# Be verbose when runing under a CI context
CI_RUN = (
    os.environ.get("JENKINS_URL")
    or os.environ.get("CI")
    or os.environ.get("DRONE") is not None
)
PIP_INSTALL_SILENT = CI_RUN is False
SKIP_REQUIREMENTS_INSTALL = "SKIP_REQUIREMENTS_INSTALL" in os.environ
EXTRA_REQUIREMENTS_INSTALL = os.environ.get("EXTRA_REQUIREMENTS_INSTALL")

COVERAGE_VERSION_REQUIREMENT = "coverage==5.2"

# Prevent Python from writing bytecode
os.environ["PYTHONDONTWRITEBYTECODE"] = "1"

# Global Path Definitions
REPO_ROOT = pathlib.Path(__file__).resolve().parent

# Change current directory to REPO_ROOT
os.chdir(REPO_ROOT)

ARTIFACTS_DIR = REPO_ROOT / "artifacts"
# Make sure the artifacts directory exists
ARTIFACTS_DIR.mkdir(parents=True, exist_ok=True)
RUNTESTS_LOGFILE = (
    ARTIFACTS_DIR
    / "runtests-{}.log".format(datetime.datetime.now().strftime("%Y%m%d%H%M%S.%f"))
).relative_to(REPO_ROOT)
COVERAGE_REPORT_DB = ARTIFACTS_DIR / ".coverage"
COVERAGE_REPORT_PROJECT = (ARTIFACTS_DIR / "coverage" / "project.xml").relative_to(
    REPO_ROOT
)
COVERAGE_REPORT_TESTS = (ARTIFACTS_DIR / "coverage" / "tests.xml").relative_to(
    REPO_ROOT
)
JUNIT_REPORT = (ARTIFACTS_DIR / "junit-report.xml").relative_to(REPO_ROOT)


def _get_session_python_version_info(session):
    try:
        version_info = session._runner._real_python_version_info
    except AttributeError:
        session_py_version = session.run_always(
            "python",
            "-c" 'import sys; sys.stdout.write("{}.{}.{}".format(*sys.version_info))',
            silent=True,
            log=False,
        )
        version_info = tuple(
            int(part) for part in session_py_version.split(".") if part.isdigit()
        )
        session._runner._real_python_version_info = version_info
    return version_info


def _get_pydir(session):
    version_info = _get_session_python_version_info(session)
    if version_info < (3, 9):
        session.error("Only Python >= 3.9 is supported")
    return "py{}.{}".format(*version_info)


@nox.session
def coverage(session):
    """
    Coverage analysis.
    """
    session.install(COVERAGE_VERSION_REQUIREMENT, silent=PIP_INSTALL_SILENT)
    # Always combine and generate the XML coverage report
    try:
        session.run("coverage", "combine")
    except CommandFailed:
        # Sometimes some of the coverage files are corrupt which would
        # trigger a CommandFailed exception
        pass
    # Generate report for salt code coverage
    session.run(
        "coverage",
        "xml",
        "-o",
        str(COVERAGE_REPORT_PROJECT),
        "--omit=tests/*",
        "--include=tiamat/*",
    )
    # Generate report for tests code coverage
    session.run(
        "coverage",
        "xml",
        "-o",
        str(COVERAGE_REPORT_TESTS),
        "--omit=tiamat/*",
        "--include=tests/*",
    )
    # Move the coverage DB to artifacts/coverage in order for it to be archived by CI
    if pathlib.Path(".coverage").exists():
        shutil.move(".coverage", str(COVERAGE_REPORT_DB))


@nox.session(python=PYTHON_VERSIONS)
def tests(session):
    if SKIP_REQUIREMENTS_INSTALL is False:
        session.install(COVERAGE_VERSION_REQUIREMENT, silent=PIP_INSTALL_SILENT)

        # Install requirements
        requirements_file = (
            REPO_ROOT / "requirements" / _get_pydir(session) / "tests.txt"
        )
        install_command = [
            "--progress-bar=off",
            "-r",
            str(requirements_file.relative_to(REPO_ROOT)),
        ]
        session.install(*install_command, silent=PIP_INSTALL_SILENT)

        if EXTRA_REQUIREMENTS_INSTALL:
            session.log(
                "Installing the following extra requirements because the "
                "EXTRA_REQUIREMENTS_INSTALL environment variable was set: "
                "EXTRA_REQUIREMENTS_INSTALL='%s'",
                EXTRA_REQUIREMENTS_INSTALL,
            )
            install_command = ["--progress-bar=off"]
            install_command += [
                req.strip() for req in EXTRA_REQUIREMENTS_INSTALL.split()
            ]
            session.install(*install_command, silent=PIP_INSTALL_SILENT)

    session.run("coverage", "erase")
    args = [
        "--rootdir",
        str(REPO_ROOT),
        f"--log-file={RUNTESTS_LOGFILE}",
        "--log-file-level=debug",
        "--show-capture=no",
        f"--junitxml={JUNIT_REPORT}",
        "--showlocals",
        "-ra",
        "-s",
    ]
    if session._runner.global_config.forcecolor:
        args.append("--color=yes")
    if not session.posargs:
        args.append("tests/")
    else:
        for arg in session.posargs:
            if arg.startswith("--color") and args[0].startswith("--color"):
                args.pop(0)
            args.append(arg)
    session.run("coverage", "run", "-m", "pytest", *args)
    session.notify("coverage")


@nox.session(python="3")
@nox.parametrize("compress", [False, True])
def docs(session, compress):
    """
    Build Salt's Documentation
    """
    session.notify(f"docs-html-{session.python}(compress={compress})")


@nox.session(name="docs-html", python="3.10")
@nox.parametrize("compress", [False, True])
def docs_html(session, compress):
    """
    Build Salt's HTML Documentation
    """

    requirements_file = REPO_ROOT / "requirements" / _get_pydir(session) / "docs.txt"
    install_command = [
        "--progress-bar=off",
        "-r",
        str(requirements_file.relative_to(REPO_ROOT)),
    ]
    session.install(*install_command, silent=PIP_INSTALL_SILENT)
    os.chdir("docs/")
    session.run("make", "clean", external=True)
    session.run("make", "html", "SPHINXOPTS=-W", external=True)
    if compress:
        session.run("tar", "-cJvf", "html-archive.tar.xz", "_build/html", external=True)
    os.chdir("..")
