============
Tiamat 8.0.0
============

Welcome to tiamat version 8.0.0's release notes!
This version of tiamat introduces pluggable backends.
Tiamat supports a `pyinstaller` backend out of the box.
This is the default and supported way to use tiamat.
You can easily extend tiamat to use `other backends <https://gitlab.com/saltstack/pop/tiamat/-/issues/23>`_.
Here we will discuss how to extend tiamat with an alternative backend.

Pluggable Backends
==================

Getting started
---------------

You can use a tool called `pop-create` to auto-generate the structure for your project.

.. code-block:: bash

    # Create a new directory for your tiamat backend
    $ mkdir tiamat-[my-backend]

    # Move into that directory
    $ cd tiamat-[my-backend]

    # Install the pop-create project for initializing boilerplate code
    $ pip install pop-create

    # Run pop-create -- telling it that your project will vertically extend tiamat
    $ pop-create -n tiamat-[my-backend] -tv -d builder tool

    # Add tiamat to the requirements of your new project
    $ echo "tiamat>=8.0.0" >> requirements/base.txt

    # Create the directory where all of your plugin's code will reside (use underscores instead of dashes here)
    $ mkdir tiamat_[my_backend]/builder/[my_backend].py

    # Create a place to put your helper functions
    $ mkdir tiamat_[my_backend]/tool/[my_backend]/init.py

All the files you could possibly need are now created

conf.py
-------

Here we will discuss what goes into `tiamat_my_backend/conf.py`

This is how to add your own subcommand to tiamat that is aware of options specific to your backend.

.. code-block:: python

    # In this dictionary goes all the immutable values you want to show up under hub.OPT.tiamat
    CONFIG = {
        # This option will become available to only your specific backend
        "my_option1": {"dyne": "tiamat", "subcommands": ["my-backend"]},
        # This option will become available to every subcommand
        "my_option2": {"dyne": "tiamat", "subcommands": ["_global_"]},
    }

    # This is how we make the main tiamat tool able to use our backend as a subcommand
    # This is required and the "my-backend" must match with the name of your folder under the tiamat directory
    SUBCOMMANDS = {"my-backend": {"dyne": "tiamat", "help": "my build backend for tiamat"}}

    # Include keys from the CONFIG dictionary that you want to expose on the cli
    # The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
    CLI_CONFIG = {
        # This option will become available to only your specific backend
        "my_option1": {"dyne": "tiamat", "subcommands": ["my-backend"]},
        # This option will become available to every subcommand
        "my_option2": {"dyne": "tiamat", "subcommands": ["_global_"]},
    }

    # This tells pop that "builder" is vertically extended through the folder called "builder" in your project root
    # builder plugins are the backend of tiamat
    DYNE = {"builder": ["builder"]}


builder/my_backend.py
---------------------

.. code-block:: python

    def new(hub, *, my_option1, my_option2, **opts) -> dict[str, any]:
        """
        Collect all the arguments from the opts needed for a build.
        Here we can do more complex computation of defaults and bulletproofing.
        """
        return dict(
            my_option1=my_option1,
            my_option2=my_option2,
            directory=opts.directory,
        )


    def run(hub, build_name: str):
        """
        Make the calls to perform the build process using the OPTs in "build_name"
        """
        # We can retrieve the sanitized using the build name
        opts = hub.tiamat.BUILDS[build_name]

        # A generic virtualenv plugin can be created within which to run the build
        hub.virtualenv.init.create(build_name)

        # Run the commands listed in the 'make' portion of the build config
        hub.tiamat.make.apply(build_name)

        # Helper functions can be called from hub.tool
        # This allows for organization of code so that everything doesn't need to be crammed into one giant file
        hub.tool.my_backend.helper_plugin.helper_function(build_name)


    def clean(hub, build_name: str):
        """
        Clean up virtual environments and intermediate files after a build
        """

Usage
=====

.. code-block:: bash

    tiamat my-backend --my-option1="foo" --my-option2="bar"
