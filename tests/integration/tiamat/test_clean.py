import os


def test_all(hub, tmpdir):
    tmpdir.mkdir("dist")
    tmpdir.mkdir("build")
    tmpdir.mkdir(".pytest_cache")
    tmpdir.join("foo.log").write("")

    hub.tiamat.clean.all(tmpdir)
    assert not any(subdirs or files for _, subdirs, files in os.walk(tmpdir))
