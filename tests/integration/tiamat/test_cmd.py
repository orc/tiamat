from dict_tools.data import NamespaceDict


def test_run(hub):
    hub.OPT = NamespaceDict(tiamat=dict(dry_run=False))
    ret = hub.tiamat.cmd.run(["echo", "-n", "test"])
    assert not ret.retcode
    assert not ret.stderr
    assert ret.stdout == "test"
