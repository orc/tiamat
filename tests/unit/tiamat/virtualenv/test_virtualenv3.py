def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.virtualenv3.bin = hub.virtualenv.virtualenv3.bin
    mock_hub.virtualenv.virtualenv3.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.virtualenv.virtualenv3.create = hub.virtualenv.virtualenv3.create
    mock_hub.virtualenv.virtualenv3.create(bname)
