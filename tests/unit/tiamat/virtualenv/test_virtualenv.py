def test_bin(hub, mock_hub, bname):
    mock_hub.virtualenv.virtualenv.bin = hub.virtualenv.virtualenv.bin
    mock_hub.virtualenv.virtualenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.virtualenv.virtualenv.create = hub.virtualenv.virtualenv.create
    mock_hub.virtualenv.virtualenv.create(bname)
