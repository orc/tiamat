from unittest import mock

from dict_tools import data


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].pyenv = bname
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(retcode=0, stdout="TheRoots")
    mock_hub.virtualenv.pyenv.bin = hub.virtualenv.pyenv.bin
    mock_hub.virtualenv.pyenv.pyenv_bin.return_value = (
        "/path/to/pyenv",
        "/path/to/",
    )
    mock_hub.virtualenv.pyenv.bin(bname)


def test_create(hub, mock_hub, bname, tmp_path):
    mock_hub.tiamat.BUILDS = data.NamespaceDict(
        test_build=data.NamespaceDict(venv_dir=tmp_path, pyenv="3.9.10")
    )
    mock_hub.OPT = mock.MagicMock(tiamat=mock.MagicMock(venv_dir=tmp_path))
    mock_hub.virtualenv.pyenv.create = hub.virtualenv.pyenv.create
    mock_hub.virtualenv.pyenv.bin.return_value = [
        "ENV",
        "LIBRARY_PATH=/path/to/lib/",
        tmp_path,
    ]
    mock_hub.virtualenv.pyenv.pyenv_bin.return_value = (
        "/path/to/pyenv",
        "/path/to/",
    )
    mock_hub.virtualenv.pyenv.create(bname)
