import shutil
from unittest import mock

import pytest


@pytest.mark.skipif(not shutil.which("fpm"), reason="fpm is required to run this test")
def test_cli(hub, mock_hub):
    # Only mock the builder, everything else on the hub is real
    hub.tiamat.build.builder = mock_hub.tiamat.build.builder

    with mock.patch("os.path.abspath", return_value="test_dir"), mock.patch(
        "sys.argv",
        ["tiamat", "build", "-n", "pyproject", "--pyenv=3.9.0", "--timeout=500"],
    ), mock.patch("builtins.open"):
        hub.tiamat.init.cli()

    hub.tiamat.build.builder.assert_called_once_with(
        name="pyproject",
        requirements="requirements.txt",
        sys_site=False,
        exclude=None,
        directory="test_dir",
        pyinstaller_version="4.10",
        pyinstaller_runtime_tmpdir=None,
        venv_uninstall=(),
        datas=None,
        build=False,
        pkg={},
        onedir=False,
        pyenv="3.9.0",
        run="run.py",
        no_clean=False,
        locale_utf8=False,
        dependencies={},
        release={},
        pkg_tgt=None,
        pkg_builder="fpm",
        srcdir=None,
        system_copy_in=None,
        tgt_version=None,
        venv_plugin="builtin",
        python_bin=None,
        omit=None,
        pyinstaller_args=None,
        timeout=500,
        pip_version="latest",
        use_static_requirements=True,
    )


def test_new(hub, mock_hub, tempdir):
    mock_hub.builder.pyinstaller.new = hub.builder.pyinstaller.new
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tool.pyinstaller.requirements.compile.return_value = "test_req"
    mock_hub.tiamat.data.version.return_value = "test_version"

    name = "test_name"
    requirements = "test_reqs"
    sys_site = True
    exclude = {"test_exclude"}
    directory = "test_dir"
    venv_dir = str(tempdir.mkdtemp())
    with mock.patch("tempfile.mkdtemp", return_value=venv_dir), mock.patch(
        "os.path.abspath", return_value=directory
    ):
        result = mock_hub.builder.pyinstaller.new(
            name=name,
            requirements=requirements,
            system_site=sys_site,
            exclude=exclude,
            directory=directory,
            pyinstaller_args=["--key=1234", "-v"],
            venv_plugin="builtin",
        )

    assert result == {
        "all_paths": set(),
        "binaries": [],
        "build": None,
        "cmd": [
            f"{venv_dir}/bin/python3",
            "-B",
            "-O",
            "-m",
            "PyInstaller",
        ],
        "datas": set(),
        "dependencies": None,
        "pyinstaller_version": "4.10",
        "dir": "test_dir",
        "exclude": {"test_exclude"},
        "imports": set(),
        "is_win": False,
        "locale_utf8": False,
        "name": "test_name",
        "omit": None,
        "onedir": False,
        "pkg": None,
        "pkg_builder": None,
        "pkg_tgt": None,
        "pybin": f"{venv_dir}/bin/python3",
        "pyenv": "system",
        "pyinstaller_runtime_tmpdir": None,
        "pypi_args": [
            f"{venv_dir}/bin/test_name",
            "--log-level=INFO",
            "--noconfirm",
            "--onefile",
            "--clean",
            "--key=1234",
            "-v",
        ],
        "release": None,
        "req": "test_req",
        "requirements": "test_dir/test_reqs",
        "run": "test_dir/run.py",
        "s_path": f"{venv_dir}/bin/test_name",
        "spec": "test_dir/test_name.spec",
        "srcdir": None,
        "sys_site": True,
        "system_copy_in": None,
        "venv_dir": venv_dir,
        "venv_plugin": "builtin",
        "venv_uninstall": None,
        "vroot": f"{venv_dir}/lib",
        "timeout": 300,
        "pip_version": "latest",
        "use_static_requirements": True,
        "version": "test_version",
    }
