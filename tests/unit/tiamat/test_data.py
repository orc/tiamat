from unittest import mock


def test_version(hub):
    with mock.patch("os.path.isfile", return_value=True), mock.patch(
        "builtins.open", mock.mock_open(read_data="version='99'")
    ):
        version = hub.tiamat.data.version(directory="", name="")

    assert version == "99"
