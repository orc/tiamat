import pytest


def test_build(hub, mock_hub, bname):
    mock_hub.package.dmg.build = hub.package.dmg.build

    with pytest.raises(NotImplementedError):
        mock_hub.package.dmg.build(bname)
