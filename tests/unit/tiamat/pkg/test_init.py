import shutil

import pytest
from dict_tools import data


@pytest.mark.skipif(not shutil.which("fpm"), reason="fpm is required to run this test")
def test_build(hub, mock_hub, bname):
    mock_hub.package.init.build = hub.package.init.build
    mock_hub.OPT = data.NamespaceDict(tiamat=data.NamespaceDict(pkg_builder="fpm"))

    mock_hub.package.init.build(bname)
    mock_hub.package.fpm.build.assert_called_once_with(bname)
